# Tokyo Night theme

Tokyo Night is a dark theme for [Joplin](https://joplinapp.org/) inspired by [Tokyo Night theme](https://github.com/lokesh-krishna/Tokyo-Night-Obsidian-Theme) for [Obsidian](https://obsidian.md/).

## Installation
### 1. Install font (optional)
**Fira Code**: free monospaced font with programming ligatures available on [Github](https://github.com/tonsky/FiraCode).
### 2. Install theme
- Run `Joplin`.
- Go to `Tools` menu and click `Options`.
- Click `Appearance`.
- Select `Dark` from theme menu.
- In `Editor font family` write `Fira Code` if it is installed.
- Click `Show Advanced Settings`.
- Click `Custom stylesheet for rendered markdown` and in the editor paste the CSS from [userstyle.css](https://gitlab.com/Cliffge/joplin-tokyo-night/-/blob/main/userstyle.css) saving the file.
- Click `Custom stylesheet for Joplin-wide app styles` and in the editor paste the CSS from [userchrome.css](https://gitlab.com/Cliffge/joplin-tokyo-night/-/blob/main/userchrome.css) saving the file.
- Close and reopen Joplin to apply the changes.